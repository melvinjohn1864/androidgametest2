package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Player playingDino;
    Item movingItem;

    List<Rect> rects;

    Random rand;

    Context context;

    int previousRandom = -1;

    int randomNum = 0;

    List<Item> items;




    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();
        this.context = context;

        this.screenWidth = w;
        this.screenHeight = h;

        rects = new ArrayList<>();

        this.printScreenInfo();

        rand = new Random();



        for (int i= 1; i<=4; i++) {
            Rect lane = new Rect(100, 200*i, screenWidth - 700, 200*i + 30);
            rects.add(lane);
        }

        playingDino = new Player(context,screenWidth/2 + 600, screenHeight/2 - 100);
        items = new ArrayList<>();
        setInitialPositionOfItems();



    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        for (int i = 0; i < items.size(); i++){
            items.get(i).updateItemPosition();
        }

        if (fingerAction.equalsIgnoreCase("top")){
            playingDino.setyPosition(playingDino.getyPosition() - 20);
        }else if (fingerAction.equalsIgnoreCase("bottom")){
            playingDino.setyPosition(playingDino.getyPosition() + 20);
        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.BLACK);



            for (int i= 0; i<rects.size(); i++) {
                //Rect lane = new Rect(100, 200*i, screenWidth - 700, 200*i + 30);
                this.canvas.drawRect(rects.get(i), paintbrush);
            }


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            // draw player graphic on screen
            canvas.drawBitmap(playingDino.getImage(), playingDino.getxPosition(), playingDino.getyPosition(), paintbrush);
            for (int i = 0; i < items.size(); i++){
                canvas.drawBitmap(items.get(i).getImage(), items.get(i).getxPosition(), items.get(i).getyPosition(), paintbrush);
            }

            paintbrush.setTextSize(60);
            canvas.drawText("LIVES: " + 0,
                    screenWidth-250,
                    50,
                    paintbrush
            );

            canvas.drawText("SCORE: " + 0,
                    screenWidth-700,
                    50,
                    paintbrush
            );



            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    private void setInitialPositionOfItems() {
        for (int i = 0; i < rects.size();i++){
            int min = 0;
            //previousRandom = randomNum;
            int currentRandom = rand.nextInt((4 - min)) + min;
            //randomNum = currentRandom;
            movingItem = new Item(context,rects.get(currentRandom).left,rects.get(currentRandom).top);
            items.add(movingItem);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            //1. Get position of the tap
            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();

            //2. Compare position of tap with the middle of screen

            int middleOfScreen = this.screenHeight/2;
            if (fingerYPosition <= middleOfScreen){
                fingerAction = "top";
            }else if (fingerYPosition > middleOfScreen){
                fingerAction = "bottom";
            }
        }
        else if (userAction == MotionEvent.ACTION_UP) {

        }

        return true;
    }
}
